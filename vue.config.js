module.exports = {
  configureWebpack: {
    devServer: {
      headers: {
        "Access-Control-Allow-Origin": "https://9pvgib8rn5.execute-api.eu-central-1.amazonaws.com",
        "Access-Control-Request-Method": "POST, GET, OPTIONS"
      }
    }
  }
};
