import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      mail: '',
      tasks: []
    },
    skills: [
      {
        id: 'socializing',
        title: 'Socializing im Homeoffice',
        description: 'Im Home Office besteht weniger Kontakt und Austausch mit Kolleg:innen. Beim Kaffee nach der Mittagspause, dem informellen Austausch vor und nach Meetings oder der schnellen Runde Tischkicker haben Kolleg:innen die Möglichkeit über private Themen oder aktuelle Herausforderungen bei den täglichen Aufgaben zu sprechen. Durch diese informellen Gespräche werden Probleme gelöst und Hilfestellungen gegeben. Im Home Office finden diese Gespräche nicht statt. Das Resultat sind ungelöste Probleme - sowohl privat als auch im Berufsalltag.',
        purpose: 'Mit dieser Skill soll das Socializing unter Kollegen remote gefördert werden, dass wieder mehr über private und arbeitsbezogene Probleme gesprochen wird.'
      },
      {
        id: 'remotemeetings',
        title: 'Effektive Remote Meetings',
        description: 'Wie verlagert man von einem Tag auf den nächsten alle Tasks und Meetings ins Home Office? Manche von uns sind schon Home Office erfahren, andere noch gar nicht - die allermeisten von uns kennen das Home Office in dem Ausmaß aber sicherlich noch nicht. Schwierigkeiten die dabei auftreten können betreffen z.B. Meetings: wie führe ich ein Brainstorming durch? Wie führe ich Feedbackgespräche oder wichtige Salestermine? Wie organisiere ich einen Workshop?',
        purpose: 'In dieser Skill bekommst du jeden Tag eine neue Aufgabe, die du in jedes Remote Meeting integrieren kannst. Am Ende der Woche bist der Remote-Meeting-Junkie und willst nichts anderes mehr. ;)'
      },
      {
        id: 'trennung',
        title: 'Trennung von Arbeit und Privatem',
        description: 'Arbeiten in den eigenen vier Wänden - das klingt erstmal sehr cool. Keine lange Anfahrt zum Arbeitsplatz, die Couch ist immer in Reichweite und mal eben nebenher die Wäsche in die Waschmaschine machen. Arbeit und Privates verschmelzen dadurch immer mehr. Das bringt Vorteile, aber auch Nachteile. Nach einiger Zeit im Homeoffice kann es schwierig werden, aus dem “Arbeitsmodus” am Abend in den “Privatmodus” zu schalten. Immerhin ist die Arbeitsstätte nur wenige Meter entfernt - teilweise sogar im gleiche Raum! Auch das “Reinkommen” in die Arbeit wird immer schwerer.',
        purpose: 'In dieser Skill lernst du mit der Trennung von Privatem und deiner Arbeit umzugehen. Du erfährst, wie du deinen Berufsalltag dort von Privatem trennst, aber auch wie die neue Vermischung dir helfen kann, deine Produktivität und deine Happiness zu erhöhen.'
      },
      {
        id: 'keinarbeitsweg',
        title: 'Kein Arbeitsweg = mehr Zeit',
        description: 'Wenn man sofort vom Bett an den Schreibtisch geht, fehlt einem der Übergang bzw. das Gefühl einer klaren Trennung zwischen Arbeit und Privat. Viele Menschen pendeln normalerweise (durchschnittlich 44 Minuten am Tag) - diese Zeit kann man doch eigentlich wunderbar für etwas anderes nutzen. Wolltest du schon immer mal eine neue Sprache lernen? Ins Programmieren einsteigen? Schiebst du eine Weiterbildung schon ewig vor dich her weil einfach die Zeit fehlt? Wolltest mit Yoga anfangen? Mit der Familie frühstücken? So viele Möglichkeiten, so viel neu gewonnene Zeit!',
        purpose: ''
      },
      {
        id: 'networking',
        title: 'Networking 2.0',
        description: 'Networking ist essentiell um sich persönlich weiterzuentwickeln, stellt aber beispielsweise auch einen der Grundpfeiler im Sales dar. Hierzu eignen sich insbesondere Meet-Ups, Messen oder andere Veranstaltungen um neue Menschen kennenzulernen. Um auch in einer “digital-only” Welt zum Networking-Profi zu werden, muss man kreativ sein. Dieser Skill soll dir helfen, neue Wege in Richtung Networking 2.0 einzuschlagen!'
      }
    ]
  },
  mutations: {
    setUser (state, user) {
      state.user = user
    },
    setUserTasks (state, tasks) {
      state.user.tasks = tasks
    }
  },
  getters: {
  },
  actions: {
    registerUserWithBackendAndLogin: function (context, user) {
      console.log(user)
      console.log(user.mail)
      axios
        .get('https://9pvgib8rn5.execute-api.eu-central-1.amazonaws.com/dev/register?userID=' + user.mail + '&password=' + user.password)
        .then(function () {
          delete user.password
          localStorage.setItem('usermail', user.mail)
          context.commit('setUser', user)
        })
        .catch(console.warn)
    },
    loadLocalUser: function (context) {
      if (!context.state.user || context.state.user.mail === undefined || context.state.user.mail === '') {
        const localStorageUser = { mail: localStorage.getItem('usermail') }

        if (localStorageUser) {
          context.commit('setUser', localStorageUser)
        }
      }
    },
    refreshUserTasks: function (context) {
      let usermail = context.state.user.mail ? context.state.user.mail : localStorage.getItem('usermail')
      usermail = 'manuel'
      console.log('https://9pvgib8rn5.execute-api.eu-central-1.amazonaws.com/dev/tasks/' + usermail)
      return axios
        .get('https://9pvgib8rn5.execute-api.eu-central-1.amazonaws.com/dev/tasks/' + encodeURIComponent(usermail))
        .then(res => {
          const tasks = res.data
          console.log(tasks)
          context.commit('setUserTasks', tasks)
        })
        .catch(console.warn)
    }
  },
  modules: {
  }
})
